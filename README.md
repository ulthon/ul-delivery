# ul-delivery

#### 介绍
静态资源管理系统。支持项目管理、版本管理、文件管理、CDN管理。支持从GIT导入资源。
在我们开发的时候，经常会有通用的素材和三方库，比如通用的图标文件，jq等js，通用背景图片等。这时我们可能会把这些文件下载到项目中一起部署，或者搞一个公共站点，使用ftp管理。有时候我们也会使用cdn。但是这些方式都比较原始，而且使用的时候不方便，比如需要自己去拼接连接，本系统试图管理这些资源并且能够很方便的生成引入代码。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
